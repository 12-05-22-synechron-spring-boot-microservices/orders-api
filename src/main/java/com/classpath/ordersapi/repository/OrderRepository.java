package com.classpath.ordersapi.repository;

import com.classpath.ordersapi.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByNameContaining(String name);
    Page<com.classpath.ordersapi.dto.Order> findByAmountBetweenOrderByNameAsc(double min, double max, Pageable pageable);
    @Query("select o from Order o where o.amount > ?1 and o.amount < ?2")
    Page<com.classpath.ordersapi.dto.Order> findOrdersByPriceRange(double min, double max, Pageable pageable);
}
