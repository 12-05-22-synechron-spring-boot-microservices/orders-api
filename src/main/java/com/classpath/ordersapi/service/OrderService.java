package com.classpath.ordersapi.service;

import com.classpath.ordersapi.event.OrderEvent;
import com.classpath.ordersapi.event.OrderStatus;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.classpath.ordersapi.event.OrderStatus.ORDER_ACCEPTED;
import static java.time.LocalDateTime.now;
import static java.util.concurrent.TimeUnit.SECONDS;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final WebClient webClient;
    private final Source source;


    //@CircuitBreaker(name = "inventoryservice", fallbackMethod = "fallback")
    //@Retry(name = "inventoryservice", fallbackMethod = "fallback")
    public Order saveOrder(Order order) {
        log.info("Saving the order :: {}", order);
        Order savedOrder = this.orderRepository.save(order);
        //lets make a REST API call to inventory-microservice
      /*  Integer response = this.webClient
                                    .post()
                                    .uri("/api/inventory/")
                                    .retrieve()
                                    .bodyToMono(Integer.class)
                                    .block();

        log.info("Fetched the response from Inventory-microservice :: {} ", response);*/
        OrderEvent orderEvent = new OrderEvent(savedOrder, ORDER_ACCEPTED, now());
        Message<OrderEvent> orderEventMessage = MessageBuilder.withPayload(orderEvent).build();
        this.source.output().send(orderEventMessage);
        return savedOrder;
    }

    private Order fallback(Exception exception){
        log.error("Exception while making a request :: {}", exception.getMessage());
        return Order.builder().build();
    }

    public Map<String, Object> fetchOrders(int page, int size, String strDirection, String property){
        Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageRequest = PageRequest.of(page, size, direction, property );
        Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);

        long totalElements = pageResponse.getTotalElements();
        int totalPages = pageResponse.getTotalPages();
        int pageNumber = pageResponse.getNumber();
        List<Order> data = pageResponse.getContent();
        int numberOfElements = pageResponse.getNumberOfElements();

        LinkedHashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("total-records", totalElements);
        responseMap.put("pages", totalPages);
        responseMap.put("page-number", pageNumber);
        responseMap.put("number-of-elements", numberOfElements);
        responseMap.put("data", data);
        return responseMap;
    }

    public Order findById(long orderId){
/*
        Optional<Order> orderOptional = this.orderRepository.findById(orderId);
        if(orderOptional.isPresent()){
            return orderOptional.get();
        }
        throw new IllegalArgumentException("invalid order id");
*/
        return this.orderRepository
                        .findById(orderId)
                        .orElseThrow(() -> new IllegalArgumentException("invalid order id"));
    }

    public Map<String, Object> fetchOrdersByPriceRange(int page, int size, double min, double max){
        Pageable pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "name" );
        Page<com.classpath.ordersapi.dto.Order> pageResponse = this.orderRepository.findByAmountBetweenOrderByNameAsc(min, max, pageRequest);

        long totalElements = pageResponse.getTotalElements();
        int totalPages = pageResponse.getTotalPages();
        int pageNumber = pageResponse.getNumber();
        List<com.classpath.ordersapi.dto.Order> data = pageResponse.getContent();
        int numberOfElements = pageResponse.getNumberOfElements();

        LinkedHashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("total-records", totalElements);
        responseMap.put("pages", totalPages);
        responseMap.put("page-number", pageNumber);
        responseMap.put("number-of-elements", numberOfElements);
        responseMap.put("data", data);
        return responseMap;
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}
