package com.classpath.ordersapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

@Configuration
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                    .disable()
                .csrf()
                    .disable()
                .headers()
                    .frameOptions()
                    .disable()
                .and()
                    .authorizeRequests()
                        .antMatchers("/login**", "/logout**", "/actuator/**", "/h2-console**")
                            .permitAll()
                        .antMatchers(HttpMethod.GET, "/api/orders/**")
                            .hasAnyRole("Everyone", "admins", "super_admins")
                        .antMatchers(HttpMethod.POST, "/api/orders/**")
                            .hasAnyRole("admins", "super_admins")
                        .antMatchers(HttpMethod.DELETE, "/api/orders/**")
                            .hasAnyRole("super_admins")
                        .anyRequest()
                            .fullyAuthenticated()
                .and()
                    .oauth2ResourceServer()
                    .jwt();
    }

    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter(){
        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("groups");
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }
}
