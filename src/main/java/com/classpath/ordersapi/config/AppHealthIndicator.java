package com.classpath.ordersapi.config;

import com.classpath.ordersapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class AppHealthIndicator implements HealthIndicator {
    private final OrderRepository orderRepository;

    @Override
    public Health health() {
        try {
            this.orderRepository.count();
            return Health.up().withDetail("DB-Status", "DB is UP").build();
        } catch (Exception exception){
            return Health.down().withDetail("DB-Status", "DB is down").build();
        }
    }
}

@Configuration
class KafkaHealthIndicator implements HealthIndicator {


    @Override
    public Health health() {
        try {
            return Health.up().withDetail("Kafka-Status", "Kafka is UP").build();
        } catch (Exception exception){
            return Health.down().withDetail("Kafka-Status", "Kafka is down").build();
        }
    }
}
