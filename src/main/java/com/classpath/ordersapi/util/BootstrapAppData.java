package com.classpath.ordersapi.util;

import com.classpath.ordersapi.model.LineItem;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

@Configuration
@RequiredArgsConstructor
public class BootstrapAppData {

    private final OrderRepository orderRepository;

    @Value("${app.order-count}")
    private int orderCount;
    @Value("${app.line-items}")
    private int lineItemsCount;


    @EventListener
    @Transactional
    public void onApplicationReadyEvent(ApplicationReadyEvent readyEvent){
        System.out.println("==================Application is ready ========================");
        Faker faker = new Faker();
        for(int i= 0; i < orderCount ; i++){
            Order order = Order.builder()
                                    .email(faker.internet().emailAddress())
                                    .name(faker.name().fullName())
                                    .orderDate(faker.date().past(2, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                                    .build();

            for (int j = 0; j < faker.number().numberBetween(1, lineItemsCount); j ++) {
                    // new Order (1,3, true, false, "Ram" , " Mohan", 23 , 56)
                LineItem lineItem = LineItem
                        .builder()
                        .qty(faker.number().numberBetween(2, 5))
                        .price(faker.number().randomDouble(2, 3000, 5000))
                        .name(faker.commerce().productName())
                        .build();
                order.addLineItem(lineItem);
            }
            order.setAmount(order.getLineItems().stream().map(lineItem -> lineItem.getPrice() * lineItem.getQty()).reduce(0D, Double::sum));
            this.orderRepository.save(order);
        }
     }
}
