package com.classpath.ordersapi.util;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import java.util.Arrays;

@Configuration
public class BeanConfiguration implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public BeanConfiguration(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void run(String... args) throws Exception {
        String[] beanDefinitionNames = this.applicationContext.getBeanDefinitionNames();
        Arrays.asList(beanDefinitionNames)
                .stream()
                .filter(bean -> bean.contains("user"))
                .forEach(bean -> System.out.println(bean));
    }
}
