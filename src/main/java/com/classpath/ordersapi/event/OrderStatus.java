package com.classpath.ordersapi.event;

public enum OrderStatus {
    ORDER_ACCEPTED,
    ORDER_REJECTED,
    ORDER_CANCELLED
}
