package com.classpath.ordersapi.event;

import com.classpath.ordersapi.model.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class OrderEvent {
    private Order order;
    private OrderStatus orderStatus;
    private LocalDateTime orderTime;
}
