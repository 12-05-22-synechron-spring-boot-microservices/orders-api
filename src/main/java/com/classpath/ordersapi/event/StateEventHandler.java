package com.classpath.ordersapi.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@Slf4j
public class StateEventHandler {

    @EventListener
    public void handleLivenessStateChangeHandler(AvailabilityChangeEvent<LivenessState> livenessStateAvailabilityChangeEvent){
        log.info("Liveness state change event handler :: {}", livenessStateAvailabilityChangeEvent.getState().toString().toUpperCase());
    }
    @EventListener
    public void handleReadinessStateChangeHandler(AvailabilityChangeEvent<ReadinessState> readinessStateAvailabilityChangeEvent){
        log.info("Readiness state change event handler :: {}", readinessStateAvailabilityChangeEvent.getState().toString().toUpperCase());
    }
}
