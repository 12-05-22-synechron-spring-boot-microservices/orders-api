package com.classpath.ordersapi.dto;

public interface Order {
    long getId();
    String getName();
    String getEmail();
    double getAmount();
}
