package com.classpath.ordersapi.controller;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/orders")
public class OrderRestController {
    private final OrderService orderService;

    public OrderRestController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    public Map<String, Object> fetchOrders(
            @RequestParam(name = "page", defaultValue = "0", required = false) int page,
            @RequestParam(name = "size", defaultValue = "10", required = false) int size,
            @RequestParam(name = "order", defaultValue = "asc", required = false) String direction,
            @RequestParam(name = "field", defaultValue = "name", required = false) String property
    ){

        return this.orderService.fetchOrders(page, size, direction, property);
    }

    @GetMapping("/price")
    public Map<String, Object> fetchOrdersByPriceRange(
            @RequestParam(name = "page", defaultValue = "0", required = false) int page,
            @RequestParam(name = "size", defaultValue = "10", required = false) int size,
            @RequestParam(name = "min", defaultValue = "6000", required = false) double min,
            @RequestParam(name = "max", defaultValue = "15000", required = false) double max
    ){
        return this.orderService.fetchOrdersByPriceRange(page, size, min, max);
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long id){
        return this.orderService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Order saveOrder(@RequestBody @Valid Order order){
        return this.orderService.saveOrder(order);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long id){
        this.orderService.deleteOrderById(id);
    }
}
