package com.classpath.ordersapi.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/state")
@RequiredArgsConstructor
public class StateRestController {

    private final ApplicationAvailability applicationAvailability;
    private final ApplicationEventPublisher applicationEventPublisher;


    @PostMapping("/liveness")
    public Map<String, Object> updateLiveness(){
        LivenessState currentLivenessState = this.applicationAvailability.getLivenessState();
        //toggle the liveness state
        LivenessState updatedLivenessState = (currentLivenessState == LivenessState.CORRECT) ? LivenessState.BROKEN : LivenessState.CORRECT;
        AvailabilityChangeEvent.publish(applicationEventPublisher, updatedLivenessState.toString(), updatedLivenessState);
        Map<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("state", updatedLivenessState.toString().toUpperCase());
        return responseMap;
    }
    @PostMapping("/readiness")
    public Map<String, Object> updateReadiness(){
        ReadinessState currentReadinessState = this.applicationAvailability.getReadinessState();
        //toggle the liveness state
        ReadinessState updatedReadinessState = (currentReadinessState == ReadinessState.ACCEPTING_TRAFFIC) ? ReadinessState.REFUSING_TRAFFIC : ReadinessState.ACCEPTING_TRAFFIC;
        AvailabilityChangeEvent.publish(applicationEventPublisher, updatedReadinessState.toString(), updatedReadinessState);
        Map<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("state", updatedReadinessState.toString().toUpperCase());
        return responseMap;
    }

    @GetMapping("/liveness")
    public Map<String, Object> getLiveness(){
        LivenessState currentLivenessState = this.applicationAvailability.getLivenessState();
        Map<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("state", currentLivenessState.toString().toUpperCase());
        return responseMap;
    }
    @GetMapping("/readiness")
    public Map<String, Object> getReadiness(){
        ReadinessState currentReadinessState = this.applicationAvailability.getReadinessState();
        Map<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("state", currentReadinessState.toString().toUpperCase());
        return responseMap;
    }
}
