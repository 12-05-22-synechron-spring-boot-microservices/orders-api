package com.classpath.ordersapi.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(NOT_FOUND)
    public Error handleInvalidOrderId(IllegalArgumentException exception){
      log.error("Invalid order id passed :: {} ", exception.getMessage());
      return new Error(200, exception.getMessage());

    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public Map<String, Set<String>> handleInvalidData(MethodArgumentNotValidException exception){
      log.error("Invalid order id passed :: {} ", exception.getMessage());

        List<ObjectError> allErrors = exception.getAllErrors();
        Set<String> errorMessages = allErrors
                                .stream().map(ex -> ex.getDefaultMessage())
                                .collect(Collectors.toSet());
        Map<String, Set<String>> resultMap = new LinkedHashMap<>();
        resultMap.put("errors", errorMessages);
        return resultMap;
    }
}
@Getter
@AllArgsConstructor
class Error {
    private int code;
    private String message;
}
